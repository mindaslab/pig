require 'git'
require 'tty-prompt'
require_relative 'constants.rb'
require_relative 'lib.rb'

prompt = TTY::Prompt.new(interrupt: :exit)

loop do
  blog = prompt.ask 'pig>'

  if blog
    write_into_file blog
    publish
  end
end
