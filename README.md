# pig

![](https://freesvg.org/img/Pig4.png)

Pig is a **p**ersonal m**i**cro blo**g** app written in Ruby.

## Getting Started

Install Ruby 2.7.1.

Clone this app

`$ git clone git@gitlab.com:mindaslab/pig.git`

Set up your output directory

```
$ mkdir pmg_output; cd pmg_output
$ touch index.txt
$ touch content.txt
$ touch header.txt
$ touch .gitlab-ci.yml
```

Your final directory structure should look like this

```
/
 |-pig/
 |-pmg_output/
   |-index.txt
   |-content.txt
   |-header.txt
   |-.gitlab-ci.yml
```

In `.gitlab-ci.yml` paste this code

```
pages:
  stage: deploy
  script:
  - mkdir .public
  - cp -r * .public
  - mv .public public
  artifacts:
    paths:
    - /
  only:
  - master
```

Create a gitlab repo and in `pmg_output/` do the following

```
$ git init
$ git remote add origin <repo git url>`
$ git add -A
$ git commit -am 'initial commit'
```

Now goto `pig/` and run `ruby blog.rb` you will get a command prompt `pig>` to start micro blogging.

To find out your url, goto Gitlab  repo, in Settings --> Pages you will get an url, your blog will be at `<url>/index.txt`

