def get_file_content file
  contents = nil

  File.open file do |f|
    contents = f.readlines.join ''
  end

  contents
end

def blog_content
  get_file_content BLOG_FILE
end

def header_content
  get_file_content HEADER_FILE
end

def form_index
  index_contents = header_content + blog_content

  File.open INDEX_FILE, 'w' do |f|
    f.puts index_contents
  end
end

def write_into_file blog
  previous_content = blog_content

  File.open BLOG_FILE, 'w' do |f|
    f.puts blog
    f.puts Time.now
    f.puts "=" * 50
    f.puts
    f.puts previous_content
  end

  form_index
end

def publish
  g = Git.open(OUTPUT_DIRECTORY)
  g.add(all: true)
  g.commit('blog updated')
  g.push
end

